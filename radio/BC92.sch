EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_1-cache
LIBS:h_cc_nb_iot_ps-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "Quectel BC92"
Date "2021-04-21"
Rev "1"
Comp "WISE FARM"
Comment1 " NB_IoT  Module "
Comment2 "Vitor Canosa"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4250 1650 4250 1800
$Comp
L custom:BC92 U?
U 1 1 60A6BA14
P 4600 3150
AR Path="/60A6BA14" Ref="U?"  Part="1" 
AR Path="/60C56809/60A6BA14" Ref="U4"  Part="1" 
AR Path="/6114C59C/60A6BA14" Ref="U?"  Part="1" 
AR Path="/61CCC1A5/60A6BA14" Ref="U?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA14" Ref="U?"  Part="1" 
AR Path="/60BA0495/60A6BA14" Ref="U?"  Part="1" 
F 0 "U4" H 4600 3200 50  0000 C CNN
F 1 "BC92" H 4600 3100 50  0000 C CNN
F 2 "custom:BC92" H 5150 3150 50  0001 C CNN
F 3 "" H 5150 3150 50  0001 C CNN
	1    4600 3150
	1    0    0    -1  
$EndComp
Text Label 4150 1650 1    50   ~ 0
ANT_IMN
Wire Wire Line
	4150 1800 4150 1650
Wire Wire Line
	4350 1650 4350 1800
Wire Wire Line
	4650 1650 4650 1800
Wire Wire Line
	4750 1650 4750 1800
Wire Wire Line
	5950 2400 5800 2400
Wire Wire Line
	5950 2900 5800 2900
Wire Wire Line
	3200 3100 3350 3100
Wire Wire Line
	3200 2900 3350 2900
Wire Wire Line
	3200 2500 3350 2500
$Comp
L Device:R R?
U 1 1 60A6BA22
P 3900 7350
AR Path="/60A6BA22" Ref="R?"  Part="1" 
AR Path="/60C56809/60A6BA22" Ref="R20"  Part="1" 
AR Path="/6114C59C/60A6BA22" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60A6BA22" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA22" Ref="R?"  Part="1" 
AR Path="/60BA0495/60A6BA22" Ref="R?"  Part="1" 
F 0 "R20" H 3830 7304 50  0000 R CNN
F 1 "47k" H 3830 7395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3830 7350 50  0001 C CNN
F 3 "~" H 3900 7350 50  0001 C CNN
F 4 "RS-03K1001FT" H 3900 7350 50  0001 C CNN "MPN"
F 5 "ST" H 3900 7350 50  0001 C CNN "FUNCTION"
	1    3900 7350
	0    1    -1   0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 60A6BA15
P 4100 7000
AR Path="/60A6BA15" Ref="Q?"  Part="1" 
AR Path="/60C56809/60A6BA15" Ref="Q4"  Part="1" 
AR Path="/6114C59C/60A6BA15" Ref="Q?"  Part="1" 
AR Path="/61CCC1A5/60A6BA15" Ref="Q?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA15" Ref="Q?"  Part="1" 
AR Path="/60BA0495/60A6BA15" Ref="Q?"  Part="1" 
F 0 "Q4" H 4291 7046 50  0000 L CNN
F 1 "Q_NPN_BEC" H 4291 6955 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4300 7100 50  0001 C CNN
F 3 "~" H 4100 7000 50  0001 C CNN
F 4 "PMBT3904,215" H 4100 7000 50  0001 C CNN "MPN"
	1    4100 7000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60C6A495
P 3300 7000
AR Path="/60C6A495" Ref="R?"  Part="1" 
AR Path="/60C56809/60C6A495" Ref="R19"  Part="1" 
AR Path="/6114C59C/60C6A495" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60C6A495" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60C6A495" Ref="R?"  Part="1" 
AR Path="/60BA0495/60C6A495" Ref="R?"  Part="1" 
F 0 "R19" H 3230 6954 50  0000 R CNN
F 1 "4.7k" H 3230 7045 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3230 7000 50  0001 C CNN
F 3 "~" H 3300 7000 50  0001 C CNN
	1    3300 7000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4200 7350 4200 7500
Wire Wire Line
	3150 7000 3000 7000
Wire Wire Line
	4200 6800 4200 6650
Text Label 3000 7000 2    50   ~ 0
NB_RESET
Wire Wire Line
	3750 7350 3600 7350
Wire Wire Line
	3600 7000 3450 7000
Wire Wire Line
	3600 7000 3900 7000
Connection ~ 3600 7000
Wire Wire Line
	4050 7350 4200 7350
Wire Wire Line
	4200 7200 4200 7350
Wire Wire Line
	3600 7000 3600 7350
Connection ~ 4200 7350
$Comp
L Device:C C?
U 1 1 60A6BA17
P 8900 2450
AR Path="/60A6BA17" Ref="C?"  Part="1" 
AR Path="/60C56809/60A6BA17" Ref="C16"  Part="1" 
AR Path="/6114C59C/60A6BA17" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60A6BA17" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA17" Ref="C?"  Part="1" 
AR Path="/60BA0495/60A6BA17" Ref="C?"  Part="1" 
F 0 "C16" H 9015 2496 50  0000 L CNN
F 1 "100nF" H 9015 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8938 2300 50  0001 C CNN
F 3 "~" H 8900 2450 50  0001 C CNN
	1    8900 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60C6DC47
P 9350 2450
AR Path="/60C6DC47" Ref="C?"  Part="1" 
AR Path="/60C56809/60C6DC47" Ref="C17"  Part="1" 
AR Path="/6114C59C/60C6DC47" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60C6DC47" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60C6DC47" Ref="C?"  Part="1" 
AR Path="/60BA0495/60C6DC47" Ref="C?"  Part="1" 
F 0 "C17" H 9465 2496 50  0000 L CNN
F 1 "33pF" H 9465 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9388 2300 50  0001 C CNN
F 3 "~" H 9350 2450 50  0001 C CNN
	1    9350 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 2300 8900 2150
Wire Wire Line
	9350 2300 9350 2150
Wire Wire Line
	8900 2600 8900 2750
Wire Wire Line
	9350 2600 9350 2750
$Comp
L Device:C C?
U 1 1 60A6BA23
P 9850 2450
AR Path="/60A6BA23" Ref="C?"  Part="1" 
AR Path="/60C56809/60A6BA23" Ref="C20"  Part="1" 
AR Path="/6114C59C/60A6BA23" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60A6BA23" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA23" Ref="C?"  Part="1" 
AR Path="/60BA0495/60A6BA23" Ref="C?"  Part="1" 
F 0 "C20" H 9965 2496 50  0000 L CNN
F 1 "10pF" H 9965 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9888 2300 50  0001 C CNN
F 3 "~" H 9850 2450 50  0001 C CNN
	1    9850 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 2300 9850 2150
Wire Wire Line
	9850 2600 9850 2750
Wire Wire Line
	8450 2600 8450 2750
Wire Wire Line
	8450 2750 8900 2750
Wire Wire Line
	8900 2750 9350 2750
Connection ~ 8900 2750
Wire Wire Line
	9350 2750 9850 2750
Connection ~ 9350 2750
Wire Wire Line
	8450 2150 8900 2150
Wire Wire Line
	8900 2150 9350 2150
Connection ~ 8900 2150
Wire Wire Line
	9850 2150 9350 2150
Connection ~ 9350 2150
Text HLabel 1050 700  0    50   Input ~ 0
VCC_R
Text HLabel 1050 850  0    50   Input ~ 0
GND_R
Wire Wire Line
	1050 700  1200 700 
Wire Wire Line
	1050 850  1200 850 
Text Label 1200 700  0    50   ~ 0
VCC_R
Text Label 1200 850  0    50   ~ 0
GND_R
Text Label 3200 2500 2    50   ~ 0
GND_R
Text Label 3200 2900 2    50   ~ 0
GND_R
Text Label 3200 3100 2    50   ~ 0
GND_R
Text Label 4250 1650 1    50   ~ 0
GND_R
Text Label 4350 1650 1    50   ~ 0
GND_R
Text Label 4650 1650 1    50   ~ 0
GND_R
Text Label 4750 1650 1    50   ~ 0
GND_R
Text Label 5950 2400 0    50   ~ 0
GND_R
Text Label 5950 2900 0    50   ~ 0
GND_R
Text Label 9050 2150 0    50   ~ 0
VCC_R
Text Label 9050 2750 0    50   ~ 0
GND_R
Text Label 4850 1650 1    50   ~ 0
VCC_R
Text Label 4950 1650 1    50   ~ 0
VCC_R
Text Label 4200 7500 0    50   ~ 0
GND_R
Wire Wire Line
	9300 950  9450 950 
Text Label 9300 950  0    60   ~ 0
ANT
$Comp
L wise_farm_nb_iot_1-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue C?
U 1 1 60A6BA3C
P 8700 1250
AR Path="/60A6BA3C" Ref="C?"  Part="1" 
AR Path="/60C56809/60A6BA3C" Ref="C15"  Part="1" 
AR Path="/6114C59C/60A6BA3C" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60A6BA3C" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA3C" Ref="C?"  Part="1" 
AR Path="/60BA0495/60A6BA3C" Ref="C?"  Part="1" 
F 0 "C15" H 8800 1250 50  0000 L CNN
F 1 "X pF" H 8725 1150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8738 1100 30  0001 C CNN
F 3 "Kit smd capacitors" H 8700 1250 60  0001 C CNN
F 4 "---" H 8700 1250 60  0001 C CNN "digikey"
	1    8700 1250
	-1   0    0    -1  
$EndComp
$Comp
L wise_farm_nb_iot_1-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue C?
U 1 1 60A6BA30
P 9300 1250
AR Path="/60A6BA30" Ref="C?"  Part="1" 
AR Path="/60C56809/60A6BA30" Ref="C18"  Part="1" 
AR Path="/6114C59C/60A6BA30" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60A6BA30" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA30" Ref="C?"  Part="1" 
AR Path="/60BA0495/60A6BA30" Ref="C?"  Part="1" 
F 0 "C18" H 9050 1250 50  0000 L CNN
F 1 "X pF" H 8950 1150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9338 1100 30  0001 C CNN
F 3 "Kit smd capacitors" H 9300 1250 60  0001 C CNN
F 4 "---" H 9300 1250 60  0001 C CNN "digikey"
	1    9300 1250
	-1   0    0    -1  
$EndComp
Text Notes 9200 650  2    60   ~ 0
Dual band PI Macthing Network
$Comp
L wise_farm_nb_iot_1-rescue:L-ln_hope_rf_p01-rescue-ble_nrf52838-rescue L?
U 1 1 61D38EEA
P 9000 950
AR Path="/61D38EEA" Ref="L?"  Part="1" 
AR Path="/60C56809/61D38EEA" Ref="L3"  Part="1" 
AR Path="/6114C59C/61D38EEA" Ref="L?"  Part="1" 
AR Path="/61CCC1A5/61D38EEA" Ref="L?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38EEA" Ref="L?"  Part="1" 
AR Path="/60BA0495/61D38EEA" Ref="L?"  Part="1" 
F 0 "L3" V 8950 950 50  0000 C CNN
F 1 "L" V 9075 950 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 9000 950 50  0001 C CNN
F 3 "Kit smd inductors" H 9000 950 50  0001 C CNN
F 4 "---" V 9000 950 60  0001 C CNN "digikey"
	1    9000 950 
	0    1    -1   0   
$EndComp
Text Label 8550 950  2    50   ~ 0
ANT_IMN
Wire Wire Line
	9300 1400 9300 1550
Wire Wire Line
	8700 1400 8700 1550
Wire Wire Line
	8700 1100 8700 950 
Wire Wire Line
	8700 950  8850 950 
Wire Wire Line
	9300 1100 9300 950 
Wire Wire Line
	9300 950  9150 950 
Connection ~ 9300 950 
$Comp
L Device:R R?
U 1 1 60A6BA19
P 6150 7350
AR Path="/60A6BA19" Ref="R?"  Part="1" 
AR Path="/60C56809/60A6BA19" Ref="R24"  Part="1" 
AR Path="/6114C59C/60A6BA19" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60A6BA19" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA19" Ref="R?"  Part="1" 
AR Path="/60BA0495/60A6BA19" Ref="R?"  Part="1" 
F 0 "R24" H 6080 7304 50  0000 R CNN
F 1 "47k" H 6080 7395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6080 7350 50  0001 C CNN
F 3 "~" H 6150 7350 50  0001 C CNN
F 4 "RS-03K1001FT" H 6150 7350 50  0001 C CNN "MPN"
F 5 "ST" H 6150 7350 50  0001 C CNN "FUNCTION"
	1    6150 7350
	0    1    -1   0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 60C99B7C
P 6350 7000
AR Path="/60C99B7C" Ref="Q?"  Part="1" 
AR Path="/60C56809/60C99B7C" Ref="Q6"  Part="1" 
AR Path="/6114C59C/60C99B7C" Ref="Q?"  Part="1" 
AR Path="/61CCC1A5/60C99B7C" Ref="Q?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60C99B7C" Ref="Q?"  Part="1" 
AR Path="/60BA0495/60C99B7C" Ref="Q?"  Part="1" 
F 0 "Q6" H 6541 7046 50  0000 L CNN
F 1 "Q_NPN_BEC" H 6541 6955 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6550 7100 50  0001 C CNN
F 3 "~" H 6350 7000 50  0001 C CNN
F 4 "PMBT3904,215" H 6350 7000 50  0001 C CNN "MPN"
	1    6350 7000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60A6BA1B
P 5550 7000
AR Path="/60A6BA1B" Ref="R?"  Part="1" 
AR Path="/60C56809/60A6BA1B" Ref="R22"  Part="1" 
AR Path="/6114C59C/60A6BA1B" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60A6BA1B" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA1B" Ref="R?"  Part="1" 
AR Path="/60BA0495/60A6BA1B" Ref="R?"  Part="1" 
F 0 "R22" H 5480 6954 50  0000 R CNN
F 1 "4.7k" H 5480 7045 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5480 7000 50  0001 C CNN
F 3 "~" H 5550 7000 50  0001 C CNN
	1    5550 7000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6450 7350 6450 7500
Wire Wire Line
	5400 7000 5250 7000
Wire Wire Line
	6450 6800 6450 6650
Text Label 5250 7000 2    50   ~ 0
NB_PWRKEY
Wire Wire Line
	6000 7350 5850 7350
Wire Wire Line
	5850 7000 5700 7000
Wire Wire Line
	5850 7000 6150 7000
Connection ~ 5850 7000
Wire Wire Line
	6300 7350 6450 7350
Wire Wire Line
	6450 7200 6450 7350
Wire Wire Line
	5850 7000 5850 7350
Connection ~ 6450 7350
Text Label 6450 7500 0    50   ~ 0
GND_R
Text HLabel 1050 1000 0    50   Input ~ 0
PWRKEY_R
Text HLabel 1050 1150 0    50   Input ~ 0
RESET_R
Wire Wire Line
	1050 1000 1200 1000
Wire Wire Line
	1050 1150 1200 1150
Wire Wire Line
	3350 3600 3200 3600
Wire Wire Line
	3350 3700 3200 3700
Text Label 6450 6650 0    50   ~ 0
PWRKEY
Text Label 4200 6650 0    50   ~ 0
RESET
Text Label 3200 3600 2    50   ~ 0
PWRKEY
Text Label 3200 3700 2    50   ~ 0
RESET
Text HLabel 1050 1450 0    50   Output ~ 0
TXD_R
Text HLabel 1050 1600 0    50   Input ~ 0
RXD_R
Wire Wire Line
	1050 1450 1200 1450
Wire Wire Line
	1050 1600 1200 1600
Text Label 1200 1150 0    50   ~ 0
NB_RESET
Text Label 1200 1000 0    50   ~ 0
NB_PWRKEY
Text Label 1200 1450 0    50   ~ 0
NB_TXD
Text Label 1200 1600 0    50   ~ 0
NB_RXD
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 61D38F04
P 1450 5950
AR Path="/61D38F04" Ref="Q?"  Part="1" 
AR Path="/60C56809/61D38F04" Ref="Q2"  Part="1" 
AR Path="/6114C59C/61D38F04" Ref="Q?"  Part="1" 
AR Path="/61CCC1A5/61D38F04" Ref="Q?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38F04" Ref="Q?"  Part="1" 
AR Path="/60BA0495/61D38F04" Ref="Q?"  Part="1" 
F 0 "Q2" H 1641 5996 50  0000 L CNN
F 1 "Q_NPN_BEC" H 1641 5905 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1650 6050 50  0001 C CNN
F 3 "~" H 1450 5950 50  0001 C CNN
F 4 "PMBT3904,215" H 1450 5950 50  0001 C CNN "MPN"
	1    1450 5950
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 61D38F05
P 1700 7300
AR Path="/61D38F05" Ref="Q?"  Part="1" 
AR Path="/60C56809/61D38F05" Ref="Q1"  Part="1" 
AR Path="/6114C59C/61D38F05" Ref="Q?"  Part="1" 
AR Path="/61CCC1A5/61D38F05" Ref="Q?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38F05" Ref="Q?"  Part="1" 
AR Path="/60BA0495/61D38F05" Ref="Q?"  Part="1" 
F 0 "Q1" H 1891 7346 50  0000 L CNN
F 1 "Q_NPN_BEC" H 1891 7255 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1900 7400 50  0001 C CNN
F 3 "~" H 1700 7300 50  0001 C CNN
F 4 "PMBT3904,215" H 1700 7300 50  0001 C CNN "MPN"
	1    1700 7300
	0    -1   1    0   
$EndComp
Wire Wire Line
	1500 7400 1200 7400
Wire Wire Line
	1200 7400 1200 7250
Wire Wire Line
	1200 6950 1200 6800
$Comp
L Device:R R?
U 1 1 61D38EEE
P 1700 6800
AR Path="/61D38EEE" Ref="R?"  Part="1" 
AR Path="/60C56809/61D38EEE" Ref="R13"  Part="1" 
AR Path="/6114C59C/61D38EEE" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/61D38EEE" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38EEE" Ref="R?"  Part="1" 
AR Path="/60BA0495/61D38EEE" Ref="R?"  Part="1" 
F 0 "R13" H 1630 6754 50  0000 R CNN
F 1 "4.7k" H 1630 6845 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1630 6800 50  0001 C CNN
F 3 "~" H 1700 6800 50  0001 C CNN
	1    1700 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 6650 1700 6500
Wire Wire Line
	1700 7100 1700 6950
Wire Wire Line
	1200 7400 1050 7400
Connection ~ 1200 7400
Text Label 1050 7400 2    50   ~ 0
NB_TXD
Text HLabel 1050 1750 0    50   Input ~ 0
VCC_MCU_R
Text HLabel 1050 2100 0    50   Input ~ 0
BJT_CTRL_R
Wire Wire Line
	1050 1750 1200 1750
Wire Wire Line
	1050 2100 1200 2100
Text Label 1200 1750 0    50   ~ 0
VCC_MCU
Text Label 1200 2100 0    50   ~ 0
BJT_CTRL
Text Label 2100 6500 2    50   ~ 0
BJT_CTRL
$Comp
L Device:C C?
U 1 1 61D38F06
P 2150 6800
AR Path="/61D38F06" Ref="C?"  Part="1" 
AR Path="/60C56809/61D38F06" Ref="C4"  Part="1" 
AR Path="/6114C59C/61D38F06" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/61D38F06" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38F06" Ref="C?"  Part="1" 
AR Path="/60BA0495/61D38F06" Ref="C?"  Part="1" 
F 0 "C4" H 2265 6846 50  0000 L CNN
F 1 "1nF" H 2265 6755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2188 6650 50  0001 C CNN
F 3 "~" H 2150 6800 50  0001 C CNN
	1    2150 6800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2150 6650 2150 6500
Wire Wire Line
	2150 6500 1700 6500
Wire Wire Line
	2150 6950 2150 7100
Wire Wire Line
	2150 7100 1700 7100
Connection ~ 1700 7100
Wire Wire Line
	1900 7400 2050 7400
Wire Wire Line
	5800 3600 5950 3600
Text Label 5950 3600 0    50   ~ 0
TXD
Text Label 2050 7400 0    50   ~ 0
TXD
$Comp
L Device:R R?
U 1 1 61D38EEF
P 1450 5450
AR Path="/61D38EEF" Ref="R?"  Part="1" 
AR Path="/60C56809/61D38EEF" Ref="R15"  Part="1" 
AR Path="/6114C59C/61D38EEF" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/61D38EEF" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38EEF" Ref="R?"  Part="1" 
AR Path="/60BA0495/61D38EEF" Ref="R?"  Part="1" 
F 0 "R15" H 1380 5404 50  0000 R CNN
F 1 "4.7k" H 1380 5495 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1380 5450 50  0001 C CNN
F 3 "~" H 1450 5450 50  0001 C CNN
	1    1450 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 61D38EF0
P 1950 5800
AR Path="/61D38EF0" Ref="R?"  Part="1" 
AR Path="/60C56809/61D38EF0" Ref="R7"  Part="1" 
AR Path="/6114C59C/61D38EF0" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/61D38EF0" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38EF0" Ref="R?"  Part="1" 
AR Path="/60BA0495/61D38EF0" Ref="R?"  Part="1" 
F 0 "R7" H 1880 5754 50  0000 R CNN
F 1 "10k" H 1880 5845 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1880 5800 50  0001 C CNN
F 3 "~" H 1950 5800 50  0001 C CNN
	1    1950 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 6050 1950 6050
Wire Wire Line
	1950 6050 1950 5950
Wire Wire Line
	1950 5650 1950 5500
$Comp
L Device:C C?
U 1 1 60D51D4D
P 1000 5450
AR Path="/60D51D4D" Ref="C?"  Part="1" 
AR Path="/60C56809/60D51D4D" Ref="C8"  Part="1" 
AR Path="/6114C59C/60D51D4D" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60D51D4D" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60D51D4D" Ref="C?"  Part="1" 
AR Path="/60BA0495/60D51D4D" Ref="C?"  Part="1" 
F 0 "C8" H 1115 5496 50  0000 L CNN
F 1 "1nF" H 1115 5405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1038 5300 50  0001 C CNN
F 3 "~" H 1000 5450 50  0001 C CNN
	1    1000 5450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1450 5300 1450 5150
Wire Wire Line
	1450 5150 1000 5150
Wire Wire Line
	1000 5150 1000 5300
Wire Wire Line
	1000 5600 1000 5750
Wire Wire Line
	1000 5750 1450 5750
Wire Wire Line
	1450 5600 1450 5750
Connection ~ 1450 5750
Text Label 1400 5150 2    50   ~ 0
BJT_CTRL
Wire Wire Line
	1950 6050 2100 6050
Connection ~ 1950 6050
Wire Wire Line
	1250 6050 1100 6050
Wire Wire Line
	4950 4500 4950 4650
Text Label 4950 4650 3    50   ~ 0
VDD_EXT
Text Label 1950 5500 0    50   ~ 0
VDD_EXT
Text Label 1100 6050 2    50   ~ 0
NB_RXD
Wire Wire Line
	5800 3700 5950 3700
Text Label 5950 3700 0    50   ~ 0
RXD
Text Label 2100 6050 0    50   ~ 0
RXD
Text Notes 1150 6200 2    50   ~ 0
(MCU_TXD)
Text Notes 1100 7550 2    50   ~ 0
(MCU_RXD)
NoConn ~ 5800 3400
Text Label 5950 2700 0    50   ~ 0
SIM1_RST
Wire Wire Line
	5800 2700 5950 2700
Wire Wire Line
	5800 2600 5950 2600
Wire Wire Line
	5800 2500 5950 2500
Wire Wire Line
	5800 2800 5950 2800
Text Label 5950 2800 0    50   ~ 0
SIM1_VCC
Text Label 5950 2500 0    50   ~ 0
SIM1_CLK
Text Label 5950 2600 0    50   ~ 0
SIM1_DATA
Wire Wire Line
	4050 1800 4050 1650
Text Label 4050 1650 1    50   ~ 0
GND_R
NoConn ~ 3350 2600
Wire Wire Line
	5050 4500 5050 4650
Text Label 5050 4650 3    50   ~ 0
PSM_EINT
NoConn ~ 4850 4500
NoConn ~ 4750 4500
NoConn ~ 4650 4500
Text Notes 8000 2000 0    50   ~ 0
The input voltage of VBAT ranges from 3.4V to 4.2V.\nThe module drains a maximum current of approx. 1.6A during GSM burst.\nThe minimum width of VBAT trace is recommended to be 2mm.
$Comp
L Device:R R?
U 1 1 60EC18F8
P 5850 6150
AR Path="/60EC18F8" Ref="R?"  Part="1" 
AR Path="/60C56809/60EC18F8" Ref="R23"  Part="1" 
AR Path="/6114C59C/60EC18F8" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60EC18F8" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60EC18F8" Ref="R?"  Part="1" 
AR Path="/60BA0495/60EC18F8" Ref="R?"  Part="1" 
F 0 "R23" H 5780 6104 50  0000 R CNN
F 1 "47k" H 5780 6195 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5780 6150 50  0001 C CNN
F 3 "~" H 5850 6150 50  0001 C CNN
F 4 "RS-03K1001FT" H 5850 6150 50  0001 C CNN "MPN"
F 5 "ST" H 5850 6150 50  0001 C CNN "FUNCTION"
	1    5850 6150
	0    1    -1   0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 60A6BA35
P 6050 5800
AR Path="/60A6BA35" Ref="Q?"  Part="1" 
AR Path="/60C56809/60A6BA35" Ref="Q5"  Part="1" 
AR Path="/6114C59C/60A6BA35" Ref="Q?"  Part="1" 
AR Path="/61CCC1A5/60A6BA35" Ref="Q?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA35" Ref="Q?"  Part="1" 
AR Path="/60BA0495/60A6BA35" Ref="Q?"  Part="1" 
F 0 "Q5" H 6241 5846 50  0000 L CNN
F 1 "Q_NPN_BEC" H 6241 5755 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6250 5900 50  0001 C CNN
F 3 "~" H 6050 5800 50  0001 C CNN
F 4 "PMBT3904,215" H 6050 5800 50  0001 C CNN "MPN"
	1    6050 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 6150 6150 6300
Wire Wire Line
	5100 5800 4950 5800
Wire Wire Line
	6150 5600 6150 5450
Text Label 4950 5800 2    50   ~ 0
NB_PSM_EINT
Wire Wire Line
	5700 6150 5550 6150
Wire Wire Line
	5550 5800 5400 5800
Wire Wire Line
	5550 5800 5850 5800
Connection ~ 5550 5800
Wire Wire Line
	6000 6150 6150 6150
Wire Wire Line
	6150 6000 6150 6150
Wire Wire Line
	5550 5800 5550 6150
Connection ~ 6150 6150
Text Label 6150 6300 0    50   ~ 0
GND_R
Text Label 6150 5450 2    50   ~ 0
PSM_EINT
$Comp
L Device:C C?
U 1 1 61D38EF4
P 3250 5950
AR Path="/61D38EF4" Ref="C?"  Part="1" 
AR Path="/60C56809/61D38EF4" Ref="C10"  Part="1" 
AR Path="/6114C59C/61D38EF4" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/61D38EF4" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38EF4" Ref="C?"  Part="1" 
AR Path="/60BA0495/61D38EF4" Ref="C?"  Part="1" 
F 0 "C10" H 3365 5996 50  0000 L CNN
F 1 "1uF" H 3365 5905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3288 5800 50  0001 C CNN
F 3 "~" H 3250 5950 50  0001 C CNN
	1    3250 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 6100 3250 6250
Wire Wire Line
	3250 5800 3250 5650
Text Label 3250 6250 0    50   ~ 0
GND_R
Text Label 3250 5650 0    50   ~ 0
VDD_EXT
Text Notes 2650 5450 0    50   ~ 0
It is recommended to use VDD_EXT for \nthe external pull-up power supply for I/O\nnd add a bypass capacitor of 2.2uF in parallel
Text Notes 7900 3100 0    50   ~ 0
These capacitor are arranged with capacitances in ascending order. \nCapacitor with lowest capacitance is closest to VBAT pin and all \ncapacitors should be as close to VBAT pin as possible.\n
Text Notes 4950 5250 0    50   ~ 0
PSM_EINT Reference Circuit
Text Notes 5000 6550 0    50   ~ 0
PWRKEY Reference Circuit
Text Notes 3000 6550 0    50   ~ 0
Reset Reference Circuit
$Comp
L Device:LED D?
U 1 1 60A6BA1E
P 2200 3100
AR Path="/60A6BA1E" Ref="D?"  Part="1" 
AR Path="/60C56809/60A6BA1E" Ref="D3"  Part="1" 
AR Path="/6114C59C/60A6BA1E" Ref="D?"  Part="1" 
AR Path="/61CCC1A5/60A6BA1E" Ref="D?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA1E" Ref="D?"  Part="1" 
AR Path="/60BA0495/60A6BA1E" Ref="D?"  Part="1" 
F 0 "D3" V 2239 2983 50  0000 R CNN
F 1 "LED" V 2148 2983 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2200 3100 50  0001 C CNN
F 3 "~" H 2200 3100 50  0001 C CNN
F 4 "LTST-C190KRKT-TH" H 2200 3100 50  0001 C CNN "MPN"
	1    2200 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 60A6BA1F
P 2200 3600
AR Path="/60A6BA1F" Ref="R?"  Part="1" 
AR Path="/60C56809/60A6BA1F" Ref="R18"  Part="1" 
AR Path="/6114C59C/60A6BA1F" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60A6BA1F" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA1F" Ref="R?"  Part="1" 
AR Path="/60BA0495/60A6BA1F" Ref="R?"  Part="1" 
F 0 "R18" H 2270 3646 50  0000 L CNN
F 1 "2.2k" H 2270 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2130 3600 50  0001 C CNN
F 3 "~" H 2200 3600 50  0001 C CNN
	1    2200 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3250 2200 3450
Wire Wire Line
	2200 3750 2200 3900
Wire Wire Line
	2200 2950 2200 2800
$Comp
L Device:R R?
U 1 1 61D38EF7
P 1900 4450
AR Path="/61D38EF7" Ref="R?"  Part="1" 
AR Path="/60C56809/61D38EF7" Ref="R17"  Part="1" 
AR Path="/6114C59C/61D38EF7" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/61D38EF7" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38EF7" Ref="R?"  Part="1" 
AR Path="/60BA0495/61D38EF7" Ref="R?"  Part="1" 
F 0 "R17" H 1830 4404 50  0000 R CNN
F 1 "47k" H 1830 4495 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1830 4450 50  0001 C CNN
F 3 "~" H 1900 4450 50  0001 C CNN
F 4 "RS-03K1001FT" H 1900 4450 50  0001 C CNN "MPN"
F 5 "ST" H 1900 4450 50  0001 C CNN "FUNCTION"
	1    1900 4450
	0    1    -1   0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 60A6BA38
P 2100 4100
AR Path="/60A6BA38" Ref="Q?"  Part="1" 
AR Path="/60C56809/60A6BA38" Ref="Q3"  Part="1" 
AR Path="/6114C59C/60A6BA38" Ref="Q?"  Part="1" 
AR Path="/61CCC1A5/60A6BA38" Ref="Q?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA38" Ref="Q?"  Part="1" 
AR Path="/60BA0495/60A6BA38" Ref="Q?"  Part="1" 
F 0 "Q3" H 2291 4146 50  0000 L CNN
F 1 "Q_NPN_BEC" H 2291 4055 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2300 4200 50  0001 C CNN
F 3 "~" H 2100 4100 50  0001 C CNN
F 4 "PMBT3904,215" H 2100 4100 50  0001 C CNN "MPN"
	1    2100 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60A6BA39
P 1300 4100
AR Path="/60A6BA39" Ref="R?"  Part="1" 
AR Path="/60C56809/60A6BA39" Ref="R14"  Part="1" 
AR Path="/6114C59C/60A6BA39" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60A6BA39" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA39" Ref="R?"  Part="1" 
AR Path="/60BA0495/60A6BA39" Ref="R?"  Part="1" 
F 0 "R14" H 1230 4054 50  0000 R CNN
F 1 "4.7k" H 1230 4145 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1230 4100 50  0001 C CNN
F 3 "~" H 1300 4100 50  0001 C CNN
	1    1300 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2200 4450 2200 4600
Wire Wire Line
	1150 4100 1000 4100
Text Label 1000 4100 2    50   ~ 0
NETLIGHT
Wire Wire Line
	1750 4450 1600 4450
Wire Wire Line
	1600 4100 1450 4100
Wire Wire Line
	1600 4100 1900 4100
Connection ~ 1600 4100
Wire Wire Line
	2050 4450 2200 4450
Wire Wire Line
	2200 4300 2200 4450
Wire Wire Line
	1600 4100 1600 4450
Connection ~ 2200 4450
Text Label 2200 4600 0    50   ~ 0
GND_R
Text Label 4150 4650 3    50   ~ 0
NETLIGHT
Text Notes 850  3450 0    50   ~ 0
Network Status Indication
Text Notes 750  4900 0    50   ~ 0
Level Translation - Transistor Solution
Text Notes 6800 5450 0    50   ~ 0
The transistor circuit solution is not suitable for applications with high baud rates exceeding 460Kbps\nIn order to ensure the effective operation of the transistor circuit, BJT_CTL needs to select the lower of the two levels\nWhen MCU_VDD<VDD_EXT, BJT_CTL=MCU_VDD. (If low-power design is required, a MCU_IO\nshould be selected as BJT_CTL. When the module enters deep sleep, MCU controls MCU_IO as low level.\nIf high baud rate is needed, it is highly recommended to install two 1nF capacitors on transistor circiuts.
Text HLabel 1050 2250 0    50   Output ~ 0
UCR_R
Wire Wire Line
	1050 2250 1200 2250
Text Label 1200 2250 0    50   ~ 0
NB_UCR
$Comp
L Connector:TestPoint TP?
U 1 1 6132A8D7
P 2500 650
AR Path="/6132A8D7" Ref="TP?"  Part="1" 
AR Path="/60C56809/6132A8D7" Ref="TP1"  Part="1" 
AR Path="/6114C59C/6132A8D7" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6132A8D7" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6132A8D7" Ref="TP?"  Part="1" 
AR Path="/60BA0495/6132A8D7" Ref="TP?"  Part="1" 
F 0 "TP1" V 2500 838 50  0000 L CNN
F 1 "TestPoint" H 2558 677 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 650 50  0001 C CNN
F 3 "~" H 2700 650 50  0001 C CNN
	1    2500 650 
	0    1    -1   0   
$EndComp
Wire Wire Line
	2500 650  2350 650 
Wire Wire Line
	2500 750  2350 750 
Wire Wire Line
	2500 850  2350 850 
$Comp
L Connector:TestPoint TP?
U 1 1 60A6BA27
P 2500 850
AR Path="/60A6BA27" Ref="TP?"  Part="1" 
AR Path="/60C56809/60A6BA27" Ref="TP9"  Part="1" 
AR Path="/6114C59C/60A6BA27" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60A6BA27" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA27" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60A6BA27" Ref="TP?"  Part="1" 
F 0 "TP9" V 2500 1038 50  0000 L CNN
F 1 "TestPoint" H 2558 877 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 850 50  0001 C CNN
F 3 "~" H 2700 850 50  0001 C CNN
	1    2500 850 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60A6BA28
P 2500 750
AR Path="/60A6BA28" Ref="TP?"  Part="1" 
AR Path="/60C56809/60A6BA28" Ref="TP2"  Part="1" 
AR Path="/6114C59C/60A6BA28" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60A6BA28" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA28" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60A6BA28" Ref="TP?"  Part="1" 
F 0 "TP2" V 2500 938 50  0000 L CNN
F 1 "TestPoint" H 2558 777 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 750 50  0001 C CNN
F 3 "~" H 2700 750 50  0001 C CNN
	1    2500 750 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60A6BA40
P 2500 950
AR Path="/60A6BA40" Ref="TP?"  Part="1" 
AR Path="/60C56809/60A6BA40" Ref="TP10"  Part="1" 
AR Path="/6114C59C/60A6BA40" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60A6BA40" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA40" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60A6BA40" Ref="TP?"  Part="1" 
F 0 "TP10" V 2500 1138 50  0000 L CNN
F 1 "TestPoint" H 2558 977 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 950 50  0001 C CNN
F 3 "~" H 2700 950 50  0001 C CNN
	1    2500 950 
	0    1    -1   0   
$EndComp
Wire Wire Line
	2500 950  2350 950 
Wire Wire Line
	2500 1050 2350 1050
$Comp
L Connector:TestPoint TP?
U 1 1 61D38EFB
P 2500 1050
AR Path="/61D38EFB" Ref="TP?"  Part="1" 
AR Path="/60C56809/61D38EFB" Ref="TP11"  Part="1" 
AR Path="/6114C59C/61D38EFB" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61D38EFB" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38EFB" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61D38EFB" Ref="TP?"  Part="1" 
F 0 "TP11" V 2500 1238 50  0000 L CNN
F 1 "TestPoint" H 2558 1077 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 1050 50  0001 C CNN
F 3 "~" H 2700 1050 50  0001 C CNN
	1    2500 1050
	0    1    -1   0   
$EndComp
Text Label 2350 650  2    50   ~ 0
NB_PWRKEY
Text Label 2350 750  2    50   ~ 0
NB_RESET
Text Label 2350 850  2    50   ~ 0
NB_TXD
Text Label 2350 950  2    50   ~ 0
NB_RXD
Text Notes 650  1900 0    50   ~ 0
(VCC for transistor level shifter)
Text Label 2350 1050 2    50   ~ 0
BJT_CTRL
Text Label 8700 1550 0    50   ~ 0
GND_R
Text Label 9300 1550 0    50   ~ 0
GND_R
Wire Wire Line
	8700 950  8550 950 
Wire Wire Line
	2500 1250 2350 1250
$Comp
L Connector:TestPoint TP?
U 1 1 60A6BA41
P 2500 1250
AR Path="/60A6BA41" Ref="TP?"  Part="1" 
AR Path="/60C56809/60A6BA41" Ref="TP13"  Part="1" 
AR Path="/6114C59C/60A6BA41" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60A6BA41" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA41" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60A6BA41" Ref="TP?"  Part="1" 
F 0 "TP13" V 2500 1438 50  0000 L CNN
F 1 "TestPoint" H 2558 1277 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 1250 50  0001 C CNN
F 3 "~" H 2700 1250 50  0001 C CNN
	1    2500 1250
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60A6BA29
P 2500 1350
AR Path="/60A6BA29" Ref="TP?"  Part="1" 
AR Path="/60C56809/60A6BA29" Ref="TP14"  Part="1" 
AR Path="/6114C59C/60A6BA29" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60A6BA29" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA29" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60A6BA29" Ref="TP?"  Part="1" 
F 0 "TP14" V 2500 1538 50  0000 L CNN
F 1 "TestPoint" H 2558 1377 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 1350 50  0001 C CNN
F 3 "~" H 2700 1350 50  0001 C CNN
	1    2500 1350
	0    1    -1   0   
$EndComp
Wire Wire Line
	2500 1350 2350 1350
Wire Wire Line
	2500 1450 2350 1450
$Comp
L Connector:TestPoint TP?
U 1 1 60A6BA21
P 2500 1450
AR Path="/60A6BA21" Ref="TP?"  Part="1" 
AR Path="/60C56809/60A6BA21" Ref="TP15"  Part="1" 
AR Path="/6114C59C/60A6BA21" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60A6BA21" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA21" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60A6BA21" Ref="TP?"  Part="1" 
F 0 "TP15" V 2500 1638 50  0000 L CNN
F 1 "TestPoint" H 2558 1477 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 1450 50  0001 C CNN
F 3 "~" H 2700 1450 50  0001 C CNN
	1    2500 1450
	0    1    -1   0   
$EndComp
Text Label 2350 1250 2    50   ~ 0
SIM1_RST
Text Label 2350 1350 2    50   ~ 0
SIM1_CLK
Text Label 2350 1450 2    50   ~ 0
SIM1_DATA
NoConn ~ 5800 3500
NoConn ~ 5800 3300
NoConn ~ 4250 4500
NoConn ~ 4350 4500
Wire Wire Line
	9850 2150 10450 2150
Connection ~ 9850 2150
Wire Wire Line
	10450 2750 9850 2750
Connection ~ 9850 2750
$Comp
L Connector:SIM_Card J?
U 1 1 619425D8
P 10050 3900
AR Path="/619425D8" Ref="J?"  Part="1" 
AR Path="/60C56809/619425D8" Ref="J1"  Part="1" 
AR Path="/6114C59C/619425D8" Ref="J?"  Part="1" 
AR Path="/61CCC1A5/619425D8" Ref="J?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/619425D8" Ref="J?"  Part="1" 
AR Path="/60BA0495/619425D8" Ref="J?"  Part="1" 
F 0 "J1" H 10680 4000 50  0000 L CNN
F 1 "SIM_Card" H 10680 3909 50  0000 L CNN
F 2 "custom:SIM_CARD_H_DS1138_06_06SS4BSR" H 10050 4250 50  0001 C CNN
F 3 " ~" H 10000 3900 50  0001 C CNN
	1    10050 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 619425DE
P 9100 4600
AR Path="/619425DE" Ref="C?"  Part="1" 
AR Path="/60C56809/619425DE" Ref="C22"  Part="1" 
AR Path="/6114C59C/619425DE" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/619425DE" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/619425DE" Ref="C?"  Part="1" 
AR Path="/60BA0495/619425DE" Ref="C?"  Part="1" 
F 0 "C22" H 9215 4646 50  0000 L CNN
F 1 "33pF" H 9215 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9138 4450 50  0001 C CNN
F 3 "~" H 9100 4600 50  0001 C CNN
	1    9100 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 619425E4
P 8650 4600
AR Path="/619425E4" Ref="C?"  Part="1" 
AR Path="/60C56809/619425E4" Ref="C21"  Part="1" 
AR Path="/6114C59C/619425E4" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/619425E4" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/619425E4" Ref="C?"  Part="1" 
AR Path="/60BA0495/619425E4" Ref="C?"  Part="1" 
F 0 "C21" H 8765 4646 50  0000 L CNN
F 1 "33pF" H 8765 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8688 4450 50  0001 C CNN
F 3 "~" H 8650 4600 50  0001 C CNN
	1    8650 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61D38F13
P 8250 4600
AR Path="/61D38F13" Ref="C?"  Part="1" 
AR Path="/60C56809/61D38F13" Ref="C19"  Part="1" 
AR Path="/6114C59C/61D38F13" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/61D38F13" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38F13" Ref="C?"  Part="1" 
AR Path="/60BA0495/61D38F13" Ref="C?"  Part="1" 
F 0 "C19" H 8365 4646 50  0000 L CNN
F 1 "33pF" H 8365 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8288 4450 50  0001 C CNN
F 3 "~" H 8250 4600 50  0001 C CNN
	1    8250 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 4750 8250 4900
Wire Wire Line
	8250 4900 8650 4900
Wire Wire Line
	9100 4900 9100 4750
Wire Wire Line
	8650 4750 8650 4900
Connection ~ 8650 4900
Wire Wire Line
	8650 4900 9100 4900
Wire Wire Line
	9100 4450 9100 4100
Wire Wire Line
	8650 3800 8650 4450
Wire Wire Line
	8250 3700 8250 4450
Wire Wire Line
	7800 3700 7200 3700
Wire Wire Line
	7350 3800 7200 3800
Wire Wire Line
	7800 4100 7200 4100
$Comp
L Device:R R?
U 1 1 6194260F
P 8900 3600
AR Path="/6194260F" Ref="R?"  Part="1" 
AR Path="/60C56809/6194260F" Ref="R28"  Part="1" 
AR Path="/6114C59C/6194260F" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/6194260F" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6194260F" Ref="R?"  Part="1" 
AR Path="/60BA0495/6194260F" Ref="R?"  Part="1" 
F 0 "R28" H 8830 3554 50  0000 R CNN
F 1 "10k" H 8830 3645 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8830 3600 50  0001 C CNN
F 3 "~" H 8900 3600 50  0001 C CNN
	1    8900 3600
	0    -1   1    0   
$EndComp
Connection ~ 8600 4100
Wire Wire Line
	8600 4100 8100 4100
Wire Wire Line
	8750 3600 8600 3600
Wire Wire Line
	8600 3600 8600 3800
$Comp
L Device:C C?
U 1 1 60A6BA45
P 9500 4600
AR Path="/60A6BA45" Ref="C?"  Part="1" 
AR Path="/60C56809/60A6BA45" Ref="C23"  Part="1" 
AR Path="/6114C59C/60A6BA45" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60A6BA45" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA45" Ref="C?"  Part="1" 
AR Path="/60BA0495/60A6BA45" Ref="C?"  Part="1" 
F 0 "C23" H 9615 4646 50  0000 L CNN
F 1 "100nF" H 9615 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9538 4450 50  0001 C CNN
F 3 "~" H 9500 4600 50  0001 C CNN
	1    9500 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 4750 9500 4900
Text Label 9150 3900 2    50   ~ 0
GND_R
NoConn ~ 9550 4000
Text Label 8550 4900 2    50   ~ 0
GND_R
Text Label 7200 3700 2    50   ~ 0
SIM1_RST
Text Label 7200 3800 2    50   ~ 0
SIM1_CLK
Text Label 7200 4100 2    50   ~ 0
SIM1_DATA
Text Label 9100 3600 0    50   ~ 0
SIM1_VCC
Wire Wire Line
	8600 3800 8600 4100
Connection ~ 8250 3700
Wire Wire Line
	8250 3700 8100 3700
Connection ~ 8650 3800
Connection ~ 9100 4100
Wire Wire Line
	9100 4100 8600 4100
Wire Wire Line
	9100 4100 9550 4100
Wire Wire Line
	8250 3700 9550 3700
Wire Wire Line
	8650 3800 9550 3800
Wire Wire Line
	9500 3600 9500 4450
Connection ~ 9500 3600
Wire Wire Line
	9500 3600 9550 3600
Wire Wire Line
	9050 3600 9400 3600
$Comp
L power:PWR_FLAG #FLG?
U 1 1 60A6BA46
P 9400 3450
AR Path="/6114C59C/60A6BA46" Ref="#FLG?"  Part="1" 
AR Path="/60C56809/60A6BA46" Ref="#FLG0105"  Part="1" 
AR Path="/61CCC1A5/60A6BA46" Ref="#FLG?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA46" Ref="#FLG?"  Part="1" 
AR Path="/60BA0495/60A6BA46" Ref="#FLG?"  Part="1" 
F 0 "#FLG0105" H 9400 3525 50  0001 C CNN
F 1 "PWR_FLAG" H 9400 3623 50  0000 C CNN
F 2 "" H 9400 3450 50  0001 C CNN
F 3 "~" H 9400 3450 50  0001 C CNN
	1    9400 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 3450 9400 3600
Connection ~ 9400 3600
Wire Wire Line
	9400 3600 9500 3600
Wire Wire Line
	9150 3900 9550 3900
Text Notes 8150 3300 0    50   ~ 0
(U)SIM1 Interface
$Comp
L Device:R R21
U 1 1 60A6BA47
P 5250 5800
AR Path="/60C56809/60A6BA47" Ref="R21"  Part="1" 
AR Path="/61CCC1A5/60A6BA47" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA47" Ref="R?"  Part="1" 
AR Path="/60BA0495/60A6BA47" Ref="R?"  Part="1" 
F 0 "R21" H 5320 5846 50  0000 L CNN
F 1 "4.7k" H 5320 5755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5180 5800 50  0001 C CNN
F 3 "~" H 5250 5800 50  0001 C CNN
	1    5250 5800
	0    -1   -1   0   
$EndComp
Text HLabel 1050 2400 0    50   Input ~ 0
PSM_EINT_R
Wire Wire Line
	1050 2400 1200 2400
Text Label 1200 2400 0    50   ~ 0
NB_PSM_EINT
Wire Wire Line
	10450 2150 10450 2300
Wire Wire Line
	10450 2600 10450 2750
Wire Wire Line
	4150 4500 4150 4650
Wire Wire Line
	4850 1800 4850 1650
Wire Wire Line
	4950 1800 4950 1650
Wire Wire Line
	3200 2400 3350 2400
Text Label 3200 2400 2    50   ~ 0
GND_R
Wire Wire Line
	9100 4900 9500 4900
Connection ~ 9100 4900
Connection ~ 8700 950 
$Comp
L Device:C C?
U 1 1 609C2E93
P 2800 5950
AR Path="/609C2E93" Ref="C?"  Part="1" 
AR Path="/60C56809/609C2E93" Ref="C11"  Part="1" 
AR Path="/6114C59C/609C2E93" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/609C2E93" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/609C2E93" Ref="C?"  Part="1" 
AR Path="/60BA0495/609C2E93" Ref="C?"  Part="1" 
F 0 "C11" H 2915 5996 50  0000 L CNN
F 1 "1uF" H 2915 5905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2838 5800 50  0001 C CNN
F 3 "~" H 2800 5950 50  0001 C CNN
	1    2800 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 5800 2800 5650
Wire Wire Line
	2800 5650 3250 5650
Wire Wire Line
	2800 6100 2800 6250
Wire Wire Line
	2800 6250 3250 6250
$Comp
L Device:R R?
U 1 1 60A6BA44
P 7950 4100
AR Path="/60A6BA44" Ref="R?"  Part="1" 
AR Path="/60C56809/60A6BA44" Ref="R27"  Part="1" 
AR Path="/6114C59C/60A6BA44" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60A6BA44" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA44" Ref="R?"  Part="1" 
AR Path="/60BA0495/60A6BA44" Ref="R?"  Part="1" 
F 0 "R27" H 7880 4054 50  0000 R CNN
F 1 "22R" H 7880 4145 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7880 4100 50  0001 C CNN
F 3 "~" H 7950 4100 50  0001 C CNN
	1    7950 4100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 60A6BA2E
P 7500 3800
AR Path="/60A6BA2E" Ref="R?"  Part="1" 
AR Path="/60C56809/60A6BA2E" Ref="R25"  Part="1" 
AR Path="/6114C59C/60A6BA2E" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60A6BA2E" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60A6BA2E" Ref="R?"  Part="1" 
AR Path="/60BA0495/60A6BA2E" Ref="R?"  Part="1" 
F 0 "R25" H 7430 3754 50  0000 R CNN
F 1 "22R" H 7430 3845 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7430 3800 50  0001 C CNN
F 3 "~" H 7500 3800 50  0001 C CNN
	1    7500 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 61D38F14
P 7950 3700
AR Path="/61D38F14" Ref="R?"  Part="1" 
AR Path="/60C56809/61D38F14" Ref="R26"  Part="1" 
AR Path="/6114C59C/61D38F14" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/61D38F14" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38F14" Ref="R?"  Part="1" 
AR Path="/60BA0495/61D38F14" Ref="R?"  Part="1" 
F 0 "R26" H 7880 3654 50  0000 R CNN
F 1 "22R" H 7880 3745 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7880 3700 50  0001 C CNN
F 3 "~" H 7950 3700 50  0001 C CNN
	1    7950 3700
	0    -1   1    0   
$EndComp
Text Notes 10350 2500 0    50   ~ 0
TVS NM
Text Notes 7550 5700 0    118  ~ 0
TVS GROUP NM IN THIS DESIGN NM
$Comp
L Device:C C?
U 1 1 60AD89DE
P 8450 2450
AR Path="/60AD89DE" Ref="C?"  Part="1" 
AR Path="/608585CB/60AD89DE" Ref="C?"  Part="1" 
AR Path="/60C56809/60AD89DE" Ref="C28"  Part="1" 
AR Path="/60BA0495/60AD89DE" Ref="C?"  Part="1" 
F 0 "C28" H 8150 2500 50  0000 L CNN
F 1 "10 uF" H 8100 2400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8488 2300 50  0001 C CNN
F 3 "~" H 8450 2450 50  0001 C CNN
	1    8450 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 2600 7950 2750
Wire Wire Line
	7950 2150 7950 2300
$Comp
L Device:C C?
U 1 1 60AE0B0D
P 7950 2450
AR Path="/60AE0B0D" Ref="C?"  Part="1" 
AR Path="/608585CB/60AE0B0D" Ref="C?"  Part="1" 
AR Path="/60C56809/60AE0B0D" Ref="C27"  Part="1" 
AR Path="/60BA0495/60AE0B0D" Ref="C?"  Part="1" 
F 0 "C27" H 7650 2500 50  0000 L CNN
F 1 "10 uF" H 7600 2400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7988 2300 50  0001 C CNN
F 3 "~" H 7950 2450 50  0001 C CNN
	1    7950 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 2600 7450 2750
Wire Wire Line
	7450 2150 7450 2300
$Comp
L Device:C C?
U 1 1 60AE8A80
P 7450 2450
AR Path="/60AE8A80" Ref="C?"  Part="1" 
AR Path="/608585CB/60AE8A80" Ref="C?"  Part="1" 
AR Path="/60C56809/60AE8A80" Ref="C26"  Part="1" 
AR Path="/60BA0495/60AE8A80" Ref="C?"  Part="1" 
F 0 "C26" H 7150 2500 50  0000 L CNN
F 1 "10 uF" H 7100 2400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7488 2300 50  0001 C CNN
F 3 "~" H 7450 2450 50  0001 C CNN
	1    7450 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2600 6950 2750
Wire Wire Line
	6950 2150 6950 2300
$Comp
L Device:C C?
U 1 1 60AF0B46
P 6950 2450
AR Path="/60AF0B46" Ref="C?"  Part="1" 
AR Path="/608585CB/60AF0B46" Ref="C?"  Part="1" 
AR Path="/60C56809/60AF0B46" Ref="C12"  Part="1" 
AR Path="/60BA0495/60AF0B46" Ref="C?"  Part="1" 
F 0 "C12" H 6650 2500 50  0000 L CNN
F 1 "10 uF" H 6600 2400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6988 2300 50  0001 C CNN
F 3 "~" H 6950 2450 50  0001 C CNN
	1    6950 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2150 7450 2150
Wire Wire Line
	7450 2150 7950 2150
Connection ~ 7450 2150
Wire Wire Line
	7950 2150 8450 2150
Connection ~ 7950 2150
Connection ~ 8450 2150
Wire Wire Line
	6950 2750 7450 2750
Wire Wire Line
	8450 2150 8450 2300
Wire Wire Line
	7450 2750 7950 2750
Connection ~ 7450 2750
Wire Wire Line
	7950 2750 8450 2750
Connection ~ 7950 2750
Connection ~ 8450 2750
Wire Wire Line
	3450 1450 3300 1450
$Comp
L Connector:TestPoint TP?
U 1 1 60BB70DB
P 3450 1450
AR Path="/60BB70DB" Ref="TP?"  Part="1" 
AR Path="/60C56809/60BB70DB" Ref="TP18"  Part="1" 
AR Path="/6114C59C/60BB70DB" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60BB70DB" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60BB70DB" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60BB70DB" Ref="TP?"  Part="1" 
F 0 "TP18" V 3450 1638 50  0000 L CNN
F 1 "TestPoint" H 3508 1477 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3650 1450 50  0001 C CNN
F 3 "~" H 3650 1450 50  0001 C CNN
	1    3450 1450
	0    1    -1   0   
$EndComp
Text Label 3300 1450 2    50   ~ 0
SIM1_DATA
Wire Wire Line
	3450 1250 3300 1250
$Comp
L Connector:TestPoint TP?
U 1 1 60BC2FB2
P 3450 1250
AR Path="/60BC2FB2" Ref="TP?"  Part="1" 
AR Path="/60C56809/60BC2FB2" Ref="TP19"  Part="1" 
AR Path="/6114C59C/60BC2FB2" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60BC2FB2" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60BC2FB2" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60BC2FB2" Ref="TP?"  Part="1" 
F 0 "TP19" V 3450 1438 50  0000 L CNN
F 1 "TestPoint" H 3508 1277 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3650 1250 50  0001 C CNN
F 3 "~" H 3650 1250 50  0001 C CNN
	1    3450 1250
	0    1    -1   0   
$EndComp
Text Label 3300 1250 2    50   ~ 0
SIM1_RST
$Comp
L Connector:TestPoint TP?
U 1 1 60BD78E2
P 3450 1350
AR Path="/60BD78E2" Ref="TP?"  Part="1" 
AR Path="/60C56809/60BD78E2" Ref="TP20"  Part="1" 
AR Path="/6114C59C/60BD78E2" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60BD78E2" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60BD78E2" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60BD78E2" Ref="TP?"  Part="1" 
F 0 "TP20" V 3450 1538 50  0000 L CNN
F 1 "TestPoint" H 3508 1377 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3650 1350 50  0001 C CNN
F 3 "~" H 3650 1350 50  0001 C CNN
	1    3450 1350
	0    1    -1   0   
$EndComp
Wire Wire Line
	3450 1350 3300 1350
Text Label 3300 1350 2    50   ~ 0
SIM1_CLK
Wire Wire Line
	6500 750  6350 750 
$Comp
L Connector:TestPoint TP?
U 1 1 60CD016A
P 6500 750
AR Path="/60CD016A" Ref="TP?"  Part="1" 
AR Path="/60C56809/60CD016A" Ref="TP56"  Part="1" 
AR Path="/6114C59C/60CD016A" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60CD016A" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60CD016A" Ref="TP?"  Part="1" 
AR Path="/608585CB/60CD016A" Ref="TP?"  Part="1" 
AR Path="/608DC206/60CD016A" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60CD016A" Ref="TP?"  Part="1" 
F 0 "TP56" V 6500 938 50  0000 L CNN
F 1 "TestPoint" H 6558 777 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6700 750 50  0001 C CNN
F 3 "~" H 6700 750 50  0001 C CNN
	1    6500 750 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60CD0170
P 6500 850
AR Path="/60CD0170" Ref="TP?"  Part="1" 
AR Path="/60C56809/60CD0170" Ref="TP57"  Part="1" 
AR Path="/6114C59C/60CD0170" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60CD0170" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60CD0170" Ref="TP?"  Part="1" 
AR Path="/608585CB/60CD0170" Ref="TP?"  Part="1" 
AR Path="/608DC206/60CD0170" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60CD0170" Ref="TP?"  Part="1" 
F 0 "TP57" V 6500 1038 50  0000 L CNN
F 1 "TestPoint" H 6558 877 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6700 850 50  0001 C CNN
F 3 "~" H 6700 850 50  0001 C CNN
	1    6500 850 
	0    1    -1   0   
$EndComp
Wire Wire Line
	6500 850  6350 850 
Wire Wire Line
	6500 950  6350 950 
$Comp
L Connector:TestPoint TP?
U 1 1 60CD0178
P 6500 950
AR Path="/60CD0178" Ref="TP?"  Part="1" 
AR Path="/60C56809/60CD0178" Ref="TP58"  Part="1" 
AR Path="/6114C59C/60CD0178" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60CD0178" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60CD0178" Ref="TP?"  Part="1" 
AR Path="/608585CB/60CD0178" Ref="TP?"  Part="1" 
AR Path="/608DC206/60CD0178" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60CD0178" Ref="TP?"  Part="1" 
F 0 "TP58" V 6500 1138 50  0000 L CNN
F 1 "TestPoint" H 6558 977 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6700 950 50  0001 C CNN
F 3 "~" H 6700 950 50  0001 C CNN
	1    6500 950 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60CD017E
P 6500 1050
AR Path="/60CD017E" Ref="TP?"  Part="1" 
AR Path="/60C56809/60CD017E" Ref="TP59"  Part="1" 
AR Path="/6114C59C/60CD017E" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60CD017E" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60CD017E" Ref="TP?"  Part="1" 
AR Path="/608585CB/60CD017E" Ref="TP?"  Part="1" 
AR Path="/608DC206/60CD017E" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60CD017E" Ref="TP?"  Part="1" 
F 0 "TP59" V 6500 1238 50  0000 L CNN
F 1 "TestPoint" H 6558 1077 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6700 1050 50  0001 C CNN
F 3 "~" H 6700 1050 50  0001 C CNN
	1    6500 1050
	0    1    -1   0   
$EndComp
Wire Wire Line
	6500 1050 6350 1050
Text Label 6350 750  2    50   ~ 0
PSM_EINT
Text Label 6350 850  2    50   ~ 0
PSM_EINT
Text Label 6350 950  2    50   ~ 0
PSM_EINT
Text Label 6350 1050 2    50   ~ 0
PSM_EINT
$Comp
L custom:ANT_CUSTOM U?
U 1 1 609AB7B3
P 9850 950
AR Path="/60B4409A/609AB7B3" Ref="U?"  Part="1" 
AR Path="/60C56809/609AB7B3" Ref="U8"  Part="1" 
AR Path="/60BA0495/609AB7B3" Ref="U?"  Part="1" 
F 0 "U8" H 9869 1085 50  0000 L CNN
F 1 "ANT_CUSTOM" H 9850 1050 50  0001 C CNN
F 2 "custom:antenna_NB_IoT_B28" H 9850 950 50  0001 C CNN
F 3 "" H 9850 950 50  0001 C CNN
	1    9850 950 
	1    0    0    -1  
$EndComp
Text Label 1200 6800 2    50   ~ 0
VCC_MCU
Text Label 2200 2800 2    50   ~ 0
VDD_EXT
Wire Wire Line
	4850 950  4700 950 
$Comp
L Connector:TestPoint TP?
U 1 1 60AC6BCF
P 4850 950
AR Path="/60AC6BCF" Ref="TP?"  Part="1" 
AR Path="/60C56809/60AC6BCF" Ref="TP30"  Part="1" 
AR Path="/6114C59C/60AC6BCF" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60AC6BCF" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60AC6BCF" Ref="TP?"  Part="1" 
AR Path="/608585CB/60AC6BCF" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60AC6BCF" Ref="TP?"  Part="1" 
F 0 "TP30" V 4850 1138 50  0000 L CNN
F 1 "TestPoint" H 4908 977 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5050 950 50  0001 C CNN
F 3 "~" H 5050 950 50  0001 C CNN
	1    4850 950 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60AC6BD5
P 4850 1050
AR Path="/60AC6BD5" Ref="TP?"  Part="1" 
AR Path="/60C56809/60AC6BD5" Ref="TP31"  Part="1" 
AR Path="/6114C59C/60AC6BD5" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60AC6BD5" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60AC6BD5" Ref="TP?"  Part="1" 
AR Path="/608585CB/60AC6BD5" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60AC6BD5" Ref="TP?"  Part="1" 
F 0 "TP31" V 4850 1238 50  0000 L CNN
F 1 "TestPoint" H 4908 1077 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5050 1050 50  0001 C CNN
F 3 "~" H 5050 1050 50  0001 C CNN
	1    4850 1050
	0    1    -1   0   
$EndComp
Wire Wire Line
	4850 1050 4700 1050
Wire Wire Line
	4850 850  4700 850 
$Comp
L Connector:TestPoint TP?
U 1 1 60AC6BDD
P 4850 850
AR Path="/60AC6BDD" Ref="TP?"  Part="1" 
AR Path="/60C56809/60AC6BDD" Ref="TP29"  Part="1" 
AR Path="/6114C59C/60AC6BDD" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60AC6BDD" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60AC6BDD" Ref="TP?"  Part="1" 
AR Path="/608585CB/60AC6BDD" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60AC6BDD" Ref="TP?"  Part="1" 
F 0 "TP29" V 4850 1038 50  0000 L CNN
F 1 "TestPoint" H 4908 877 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5050 850 50  0001 C CNN
F 3 "~" H 5050 850 50  0001 C CNN
	1    4850 850 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60AC6BE3
P 4850 750
AR Path="/60AC6BE3" Ref="TP?"  Part="1" 
AR Path="/60C56809/60AC6BE3" Ref="TP28"  Part="1" 
AR Path="/6114C59C/60AC6BE3" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60AC6BE3" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60AC6BE3" Ref="TP?"  Part="1" 
AR Path="/608585CB/60AC6BE3" Ref="TP?"  Part="1" 
AR Path="/60BA0495/60AC6BE3" Ref="TP?"  Part="1" 
F 0 "TP28" V 4850 938 50  0000 L CNN
F 1 "TestPoint" H 4908 777 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5050 750 50  0001 C CNN
F 3 "~" H 5050 750 50  0001 C CNN
	1    4850 750 
	0    1    -1   0   
$EndComp
Wire Wire Line
	4850 750  4700 750 
Text Label 4700 750  2    50   ~ 0
VDD_EXT
Text Label 4700 850  2    50   ~ 0
VDD_EXT
Text Label 4700 950  2    50   ~ 0
VDD_EXT
Text Label 4700 1050 2    50   ~ 0
VDD_EXT
NoConn ~ 5800 3200
$Comp
L Device:R R12
U 1 1 60B170C2
P 1200 7100
AR Path="/60C56809/60B170C2" Ref="R12"  Part="1" 
AR Path="/60BA0495/60B170C2" Ref="R?"  Part="1" 
F 0 "R12" H 1270 7146 50  0000 L CNN
F 1 "R" H 1270 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1130 7100 50  0001 C CNN
F 3 "~" H 1200 7100 50  0001 C CNN
	1    1200 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3800 8650 3800
Text Label 9450 3700 2    50   ~ 0
RST
Text Label 9400 3800 2    50   ~ 0
CLK
Text Label 9450 4100 2    50   ~ 0
DATA
$Comp
L custom:SIM_Card_Custom J?
U 1 1 60BCCF26
P 10100 6200
AR Path="/60BCCF26" Ref="J?"  Part="1" 
AR Path="/60C56809/60BCCF26" Ref="J2"  Part="1" 
AR Path="/6114C59C/60BCCF26" Ref="J?"  Part="1" 
AR Path="/61CCC1A5/60BCCF26" Ref="J?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60BCCF26" Ref="J?"  Part="1" 
AR Path="/60BA0495/60BCCF26" Ref="J?"  Part="1" 
F 0 "J2" H 10730 6300 50  0000 L CNN
F 1 "SIM_Card" H 10730 6209 50  0000 L CNN
F 2 "custom:SIM_CARD_WR_COM_693010020611_" H 10100 6550 50  0001 C CNN
F 3 " ~" H 10050 6200 50  0001 C CNN
	1    10100 6200
	1    0    0    -1  
$EndComp
NoConn ~ 9600 6300
Wire Wire Line
	9450 5900 9600 5900
Wire Wire Line
	9450 6000 9600 6000
Wire Wire Line
	9450 6100 9600 6100
Wire Wire Line
	9450 6200 9600 6200
Wire Wire Line
	9450 6400 9600 6400
Text Label 9450 5900 2    50   ~ 0
SIM1_VCC
Text Label 9450 6000 2    50   ~ 0
RST
Text Label 9450 6100 2    50   ~ 0
CLK
Text Label 9450 6200 2    50   ~ 0
GND_R
Text Label 9450 6400 2    50   ~ 0
DATA
Text Notes 7600 6150 0    50   ~ 0
Other sim card with different footprint
$EndSCHEMATC
