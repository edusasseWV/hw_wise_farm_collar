EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_2-cache
LIBS:Lyrelabs-LIS2DW12-BB-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:TestPoint TP?
U 1 1 60A6B9F7
P 4200 2750
AR Path="/60A6B9F7" Ref="TP?"  Part="1" 
AR Path="/60886A89/60A6B9F7" Ref="TP3"  Part="1" 
F 0 "TP3" V 4200 2938 50  0000 L CNN
F 1 "TestPoint" H 4258 2777 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4400 2750 50  0001 C CNN
F 3 "~" H 4400 2750 50  0001 C CNN
	1    4200 2750
	0    1    -1   0   
$EndComp
Wire Wire Line
	4200 2750 4050 2750
Wire Wire Line
	4200 2850 4050 2850
Wire Wire Line
	4200 2950 4050 2950
Text Label 4050 2750 2    50   ~ 0
INT1
Text Label 4050 2850 2    50   ~ 0
INT2
Text Label 4050 2950 2    50   ~ 0
SDO
$Comp
L Connector:TestPoint TP?
U 1 1 6089BE01
P 4200 2950
AR Path="/6089BE01" Ref="TP?"  Part="1" 
AR Path="/60886A89/6089BE01" Ref="TP5"  Part="1" 
F 0 "TP5" V 4200 3138 50  0000 L CNN
F 1 "TestPoint" H 4258 2977 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4400 2950 50  0001 C CNN
F 3 "~" H 4400 2950 50  0001 C CNN
	1    4200 2950
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6089BE07
P 4200 2850
AR Path="/6089BE07" Ref="TP?"  Part="1" 
AR Path="/60886A89/6089BE07" Ref="TP4"  Part="1" 
F 0 "TP4" V 4200 3038 50  0000 L CNN
F 1 "TestPoint" H 4258 2877 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4400 2850 50  0001 C CNN
F 3 "~" H 4400 2850 50  0001 C CNN
	1    4200 2850
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60A6B9FA
P 4200 3050
AR Path="/60A6B9FA" Ref="TP?"  Part="1" 
AR Path="/60886A89/60A6B9FA" Ref="TP6"  Part="1" 
F 0 "TP6" V 4200 3238 50  0000 L CNN
F 1 "TestPoint" H 4258 3077 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4400 3050 50  0001 C CNN
F 3 "~" H 4400 3050 50  0001 C CNN
	1    4200 3050
	0    1    -1   0   
$EndComp
Wire Wire Line
	4200 3050 4050 3050
Wire Wire Line
	4200 3150 4050 3150
Wire Wire Line
	4200 3250 4050 3250
$Comp
L Connector:TestPoint TP?
U 1 1 60A6B9FB
P 4200 3250
AR Path="/60A6B9FB" Ref="TP?"  Part="1" 
AR Path="/60886A89/60A6B9FB" Ref="TP8"  Part="1" 
F 0 "TP8" V 4200 3438 50  0000 L CNN
F 1 "TestPoint" H 4258 3277 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4400 3250 50  0001 C CNN
F 3 "~" H 4400 3250 50  0001 C CNN
	1    4200 3250
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6089BE1C
P 4200 3150
AR Path="/6089BE1C" Ref="TP?"  Part="1" 
AR Path="/60886A89/6089BE1C" Ref="TP7"  Part="1" 
F 0 "TP7" V 4200 3338 50  0000 L CNN
F 1 "TestPoint" H 4258 3177 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4400 3150 50  0001 C CNN
F 3 "~" H 4400 3150 50  0001 C CNN
	1    4200 3150
	0    1    -1   0   
$EndComp
Text Label 4050 3050 2    50   ~ 0
CS
Text Label 4050 3150 2    50   ~ 0
SDI
Text Label 4050 3250 2    50   ~ 0
SCL
Text Label 5300 4300 2    50   ~ 0
SDI
Wire Wire Line
	5450 4200 5300 4200
Wire Wire Line
	5450 4100 5300 4100
Wire Wire Line
	5450 4000 5300 4000
Text Label 5300 4000 2    50   ~ 0
SCL
Text Label 5300 4100 2    50   ~ 0
CS
Wire Wire Line
	5450 4300 5300 4300
Text Label 5300 4200 2    50   ~ 0
SDO
Wire Wire Line
	5950 4900 5950 5050
Wire Wire Line
	6050 4900 6050 5050
Wire Wire Line
	5950 5050 6050 5050
Wire Wire Line
	5950 5050 5850 5050
Connection ~ 5950 5050
Wire Wire Line
	5850 4900 5850 5050
Wire Wire Line
	6550 4400 6700 4400
Wire Wire Line
	6550 4500 6700 4500
Text Label 6700 4400 0    50   ~ 0
INT1
Text Label 6700 4500 0    50   ~ 0
INT2
$Comp
L Device:C C?
U 1 1 6089BE47
P 7100 3950
AR Path="/6089BE47" Ref="C?"  Part="1" 
AR Path="/60886A89/6089BE47" Ref="C2"  Part="1" 
F 0 "C2" H 7215 3996 50  0000 L CNN
F 1 "10uF" H 7215 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7138 3800 50  0001 C CNN
F 3 "~" H 7100 3950 50  0001 C CNN
	1    7100 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60A6B9FF
P 7550 3950
AR Path="/60A6B9FF" Ref="C?"  Part="1" 
AR Path="/60886A89/60A6B9FF" Ref="C3"  Part="1" 
F 0 "C3" H 7665 3996 50  0000 L CNN
F 1 "100nF" H 7665 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7588 3800 50  0001 C CNN
F 3 "~" H 7550 3950 50  0001 C CNN
	1    7550 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3800 7100 3650
Wire Wire Line
	7100 3650 7550 3650
Wire Wire Line
	7550 3800 7550 3650
Wire Wire Line
	7100 4100 7100 4250
Wire Wire Line
	7550 4100 7550 4250
$Comp
L Device:C C?
U 1 1 6089BE5A
P 4550 3950
AR Path="/6089BE5A" Ref="C?"  Part="1" 
AR Path="/60886A89/6089BE5A" Ref="C1"  Part="1" 
F 0 "C1" H 4665 3996 50  0000 L CNN
F 1 "100nF" H 4665 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4588 3800 50  0001 C CNN
F 3 "~" H 4550 3950 50  0001 C CNN
	1    4550 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3800 4550 3650
Wire Wire Line
	4550 4100 4550 4250
Text Label 4550 4250 0    50   ~ 0
GND
Wire Wire Line
	6250 3650 6250 3700
Connection ~ 7100 3650
Wire Wire Line
	6250 3650 6650 3650
Text Notes 4250 4400 0    59   ~ 0
Near to VDD_IO
Text Notes 7050 4400 0    59   ~ 0
Near to VDD
Wire Wire Line
	6150 3650 6150 3700
$Comp
L power:PWR_FLAG #FLG?
U 1 1 6089BEA5
P 6650 3500
AR Path="/6089BEA5" Ref="#FLG?"  Part="1" 
AR Path="/60886A89/6089BEA5" Ref="#FLG03"  Part="1" 
F 0 "#FLG03" H 6650 3575 50  0001 C CNN
F 1 "PWR_FLAG" H 6650 3673 50  0000 C CNN
F 2 "" H 6650 3500 50  0001 C CNN
F 3 "~" H 6650 3500 50  0001 C CNN
	1    6650 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3500 6650 3650
Connection ~ 6650 3650
Wire Wire Line
	6650 3650 7100 3650
Text HLabel 3100 3700 0    50   Input ~ 0
CS_ACC
Text Label 3250 3900 0    50   ~ 0
SDI
Wire Wire Line
	3100 3800 3250 3800
Wire Wire Line
	3100 3700 3250 3700
Wire Wire Line
	3100 3600 3250 3600
Text Label 3250 3600 0    50   ~ 0
SCL
Text Label 3250 3700 0    50   ~ 0
CS
Wire Wire Line
	3100 3900 3250 3900
Text Label 3250 3800 0    50   ~ 0
SDO
Wire Wire Line
	3100 4000 3250 4000
Wire Wire Line
	3100 4100 3250 4100
Text Label 3250 4000 0    50   ~ 0
INT1
Text Label 3250 4100 0    50   ~ 0
INT2
Text HLabel 3100 3600 0    50   Input ~ 0
SCL_ACC
Text HLabel 3100 3800 0    50   Output ~ 0
SDO_ACC
Text HLabel 3100 3900 0    50   Input ~ 0
SDI_ACC
Text HLabel 3100 4000 0    50   Output ~ 0
INT1_ACC
Text HLabel 3100 4100 0    50   Output ~ 0
INT2_ACC
Wire Wire Line
	3100 3200 3250 3200
Text Label 3250 3200 0    50   ~ 0
GND_A
Text HLabel 3100 3200 0    50   Input ~ 0
GND_ACC
Wire Wire Line
	3100 3100 3250 3100
Text Label 3250 3100 0    50   ~ 0
VCC_IO
Text HLabel 3100 3100 0    50   Input ~ 0
VDD_ACC_IO
Text Label 6950 3650 0    50   ~ 0
VCC_A
Text Label 5900 5050 0    50   ~ 0
GND_A
Text Label 7100 4250 0    50   ~ 0
GND_A
Text Label 7550 4250 0    50   ~ 0
GND_A
Wire Wire Line
	4550 3650 6150 3650
$Comp
L dk_Motion-Sensors-Accelerometers:LIS2DH12TR U?
U 1 1 6089BE2E
P 6050 4300
AR Path="/6089BE2E" Ref="U?"  Part="1" 
AR Path="/60886A89/6089BE2E" Ref="U2"  Part="1" 
F 0 "U2" H 6600 4450 60  0000 L CNN
F 1 "LIS2DH12TR" H 6400 4550 60  0000 L CNN
F 2 "digikey-footprints:VFLGA-12_2x2mm" H 6250 4500 60  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/12/c0/5c/36/b9/58/46/f2/DM00091513.pdf/files/DM00091513.pdf/jcr:content/translations/en.DM00091513.pdf" H 6250 4600 60  0001 L CNN
F 4 "497-14851-1-ND" H 6250 4700 60  0001 L CNN "Digi-Key_PN"
F 5 "LIS2DH12TR" H 6250 4800 60  0001 L CNN "MPN"
F 6 "Sensors, Transducers" H 6250 4900 60  0001 L CNN "Category"
F 7 "Motion Sensors - Accelerometers" H 6250 5000 60  0001 L CNN "Family"
F 8 "http://www.st.com/content/ccc/resource/technical/document/datasheet/12/c0/5c/36/b9/58/46/f2/DM00091513.pdf/files/DM00091513.pdf/jcr:content/translations/en.DM00091513.pdf" H 6250 5100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/stmicroelectronics/LIS2DH12TR/497-14851-1-ND/4899838" H 6250 5200 60  0001 L CNN "DK_Detail_Page"
F 10 "ACCEL 2-16G I2C/SPI 12LGA" H 6250 5300 60  0001 L CNN "Description"
F 11 "STMicroelectronics" H 6250 5400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6250 5500 60  0001 L CNN "Status"
	1    6050 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4900 5750 5050
Wire Wire Line
	5750 5050 5850 5050
Connection ~ 5850 5050
Wire Wire Line
	3100 3000 3250 3000
Text Label 3250 3000 0    50   ~ 0
VCC_A
Text HLabel 3100 3000 0    50   Input ~ 0
VDD_ACC
Text Label 5450 3650 2    50   ~ 0
VCC_IO
$EndSCHEMATC
