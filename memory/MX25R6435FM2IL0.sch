EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_1-cache
LIBS:solar_cells_6_a_p_0-cache
LIBS:ble_nrf52838-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Memory_Flash:W25Q32JVSS U14
U 1 1 602B7586
P 5700 3900
F 0 "U14" H 5500 4300 50  0000 C CNN
F 1 "MX25R6435F" H 5700 3900 50  0000 C CNN
F 2 "custom:SOIC127P790X216-8N" H 5700 3900 50  0001 C CNN
F 3 "" H 5700 3900 50  0001 C CNN
	1    5700 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3800 5200 3800
Wire Wire Line
	5050 4000 5200 4000
Wire Wire Line
	6200 3700 6350 3700
Wire Wire Line
	6200 3800 6350 3800
Wire Wire Line
	6200 4000 6350 4000
Wire Wire Line
	6200 4100 6350 4100
Wire Wire Line
	5700 4300 5700 4450
Text Label 5050 4000 2    50   ~ 0
SCLK_MEM
Text Label 5050 3800 2    50   ~ 0
CS_MEM
Text Label 6350 3700 0    50   ~ 0
SI_MEM
Text Label 6350 3800 0    50   ~ 0
SO_MEM
Wire Wire Line
	5700 3500 5700 3350
Text Label 5700 4450 0    50   ~ 0
GND_MEM
Text Label 6350 4100 0    50   ~ 0
RST_MEM
Text Label 5700 3400 2    50   ~ 0
VCC_MEM
NoConn ~ 6350 4000
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 61021BC2
P 5700 3350
F 0 "#FLG0102" H 5700 3425 50  0001 C CNN
F 1 "PWR_FLAG" H 5700 3523 50  0000 C CNN
F 2 "" H 5700 3350 50  0001 C CNN
F 3 "~" H 5700 3350 50  0001 C CNN
	1    5700 3350
	1    0    0    -1  
$EndComp
Text HLabel 3750 3550 0    50   Input ~ 0
GND_MEM
Wire Wire Line
	3750 3550 3900 3550
Text HLabel 3750 3700 0    50   Input ~ 0
CS_MEM
Wire Wire Line
	3750 3700 3900 3700
Text Label 3900 3700 0    50   ~ 0
CS_MEM
Text HLabel 3750 3800 0    50   Input ~ 0
SCLK_MEM
Wire Wire Line
	3750 3800 3900 3800
Text Label 3900 3800 0    50   ~ 0
SCLK_MEM
Text HLabel 3750 3900 0    50   Input ~ 0
SI_MEM
Wire Wire Line
	3750 3900 3900 3900
Text Label 3900 3900 0    50   ~ 0
SI_MEM
Text HLabel 3750 4000 0    50   Input ~ 0
SO_MEM
Wire Wire Line
	3750 4000 3900 4000
Text Label 3900 4000 0    50   ~ 0
SO_MEM
Text Label 3900 3550 0    50   ~ 0
GND_MEM
Text HLabel 3750 4100 0    50   Input ~ 0
RST_MEM
Wire Wire Line
	3750 4100 3900 4100
Text Label 3900 4100 0    50   ~ 0
RST_MEM
Text HLabel 3750 3450 0    50   Input ~ 0
VCC_MEM
Wire Wire Line
	3750 3450 3900 3450
Text Label 3900 3450 0    50   ~ 0
VCC_MEM
$EndSCHEMATC
