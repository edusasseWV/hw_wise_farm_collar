EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_2-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title "Power supplies for accelerometer and radio"
Date "2021-04-23"
Rev "1"
Comp "WISE FARM"
Comment1 "Vitor Canosa"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2400 2850 0    50   Input ~ 0
VIN
Wire Wire Line
	2400 2850 2550 2850
Text HLabel 2400 3050 0    50   Output ~ 0
VOUT_R
Wire Wire Line
	2400 3050 2550 3050
Text HLabel 2400 2950 0    50   Input ~ 0
GND
Wire Wire Line
	2400 2950 2550 2950
Text Label 2550 2950 0    50   ~ 0
GND
Text HLabel 2400 3250 0    50   Input ~ 0
EN_DC_DC_R
Wire Wire Line
	2400 3250 2550 3250
Text HLabel 2400 3350 0    50   Input ~ 0
EN_LDO_R
Wire Wire Line
	2400 3350 2550 3350
$Comp
L custom:TPS61030PWPR U1
U 1 1 60A80B50
P 3900 4950
F 0 "U1" H 3900 4950 50  0000 C CNN
F 1 "TPS61030PWPR" H 3900 4800 50  0000 C CNN
F 2 "custom:SOP65P640X120-17N" H 3900 5400 50  0001 C CNN
F 3 "" H 3900 5400 50  0001 C CNN
	1    3900 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 4150 2050 4450
Wire Wire Line
	2050 4150 2300 4150
$Comp
L Device:L L1
U 1 1 60A82261
P 2450 4150
F 0 "L1" V 2640 4150 50  0000 C CNN
F 1 "4.7uH / 4m" V 2549 4150 50  0000 C CNN
F 2 "custom:NRS6012T100MMGJV" H 2450 4150 50  0001 C CNN
F 3 "~" H 2450 4150 50  0001 C CNN
	1    2450 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2600 4150 2950 4150
Text Label 2950 4150 2    50   ~ 0
SW
Wire Wire Line
	3100 4250 2950 4250
Wire Wire Line
	2950 4250 2950 4150
Connection ~ 2950 4150
Wire Wire Line
	2950 4150 3100 4150
Wire Wire Line
	3100 4450 2050 4450
Wire Wire Line
	3100 4650 2950 4650
Wire Wire Line
	2050 5500 2050 5650
Text Label 2050 5650 0    60   ~ 0
GND
Wire Wire Line
	2050 4600 2050 4450
Connection ~ 2050 4450
Wire Wire Line
	2050 4900 2050 5050
Wire Wire Line
	3100 5050 2050 5050
Wire Wire Line
	2050 5050 2050 5200
Connection ~ 2050 5050
Wire Wire Line
	1600 4900 1600 5050
Wire Wire Line
	1600 4600 1600 4450
Text Label 1600 5050 2    50   ~ 0
GND
Wire Wire Line
	1100 4900 1100 5050
Wire Wire Line
	1100 4600 1100 4450
$Comp
L Device:C C?
U 1 1 60AB8EE0
P 1100 4750
AR Path="/60AB8EE0" Ref="C?"  Part="1" 
AR Path="/608585CB/60AB8EE0" Ref="C5"  Part="1" 
F 0 "C5" H 800 4800 50  0000 L CNN
F 1 "10 uF" H 750 4700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1138 4600 50  0001 C CNN
F 3 "~" H 1100 4750 50  0001 C CNN
	1    1100 4750
	1    0    0    -1  
$EndComp
Text Label 1100 5050 2    50   ~ 0
GND
Wire Wire Line
	1600 4450 1100 4450
Connection ~ 1600 4450
Wire Wire Line
	1100 4450 950  4450
Connection ~ 1100 4450
Text Label 950  4450 2    60   ~ 0
VBATT
Wire Wire Line
	3100 5350 2950 5350
Text Label 2950 5400 2    50   ~ 0
GND
$Comp
L Device:R R?
U 1 1 60ACA67E
P 5300 4450
AR Path="/60ACA67E" Ref="R?"  Part="1" 
AR Path="/608585CB/60ACA67E" Ref="R3"  Part="1" 
F 0 "R3" H 5370 4496 50  0000 L CNN
F 1 "2M" H 5370 4405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5230 4450 50  0001 C CNN
F 3 "~" H 5300 4450 50  0001 C CNN
	1    5300 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60ACA684
P 5300 5050
AR Path="/60ACA684" Ref="R?"  Part="1" 
AR Path="/608585CB/60ACA684" Ref="R5"  Part="1" 
F 0 "R5" H 5370 5096 50  0000 L CNN
F 1 "220k" H 5370 5005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5230 5050 50  0001 C CNN
F 3 "~" H 5300 5050 50  0001 C CNN
	1    5300 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 5200 5300 5350
Text Label 5300 5350 0    60   ~ 0
GND
Wire Wire Line
	5300 4150 5300 4300
Wire Wire Line
	4700 4350 4850 4350
Wire Wire Line
	4850 4350 4850 4250
Wire Wire Line
	4850 4250 4700 4250
Wire Wire Line
	4850 4250 4850 4150
Wire Wire Line
	4850 4150 4700 4150
Connection ~ 4850 4250
Wire Wire Line
	4850 4150 5300 4150
Connection ~ 4850 4150
Wire Wire Line
	5300 4600 5300 4750
Wire Wire Line
	4700 4750 5300 4750
Connection ~ 5300 4750
Wire Wire Line
	5300 4750 5300 4900
Wire Wire Line
	5750 4600 5750 4750
Wire Wire Line
	5750 4300 5750 4150
$Comp
L Device:C C?
U 1 1 60AEE5F2
P 5750 4450
AR Path="/60AEE5F2" Ref="C?"  Part="1" 
AR Path="/608585CB/60AEE5F2" Ref="C7"  Part="1" 
F 0 "C7" H 5450 4500 50  0000 L CNN
F 1 "2.2 uF" H 5400 4400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5788 4300 50  0001 C CNN
F 3 "~" H 5750 4450 50  0001 C CNN
	1    5750 4450
	-1   0    0    -1  
$EndComp
Text Label 5750 4750 0    50   ~ 0
GND
Connection ~ 5750 4150
Wire Wire Line
	5300 4150 5750 4150
Connection ~ 5300 4150
Wire Wire Line
	1600 4450 2050 4450
Wire Wire Line
	3100 5250 2950 5250
Wire Wire Line
	2950 5250 2950 5350
Wire Wire Line
	2950 5350 2950 5450
Wire Wire Line
	2950 5450 3100 5450
Connection ~ 2950 5350
Wire Wire Line
	2950 5550 3100 5550
Wire Wire Line
	4700 5650 4850 5650
Wire Wire Line
	4850 5650 4850 5750
Wire Wire Line
	4850 5750 4700 5750
Text Label 4850 5700 0    50   ~ 0
GND
$Comp
L custom:TPS79601DCQR U3
U 1 1 60B4FA8F
P 8900 4250
F 0 "U3" H 8900 4637 60  0000 C CNN
F 1 "TPS79601DCQR" H 8900 4531 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-6" H 8900 4531 60  0001 C CNN
F 3 "" H 8900 4250 60  0000 C CNN
	1    8900 4250
	1    0    0    -1  
$EndComp
Text Notes 3500 3800 0    50   ~ 0
Boost DC/DC converter\nIN=3V\nOUT=4.5V
Wire Wire Line
	7600 4600 7600 4750
Wire Wire Line
	7600 4300 7600 4150
$Comp
L Device:C C?
U 1 1 60B51747
P 7600 4450
AR Path="/60B51747" Ref="C?"  Part="1" 
AR Path="/608585CB/60B51747" Ref="C13"  Part="1" 
F 0 "C13" H 7300 4500 50  0000 L CNN
F 1 "2.2 uF" H 7250 4400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7638 4300 50  0001 C CNN
F 3 "~" H 7600 4450 50  0001 C CNN
	1    7600 4450
	-1   0    0    -1  
$EndComp
Text Label 7600 4750 0    50   ~ 0
GND
Wire Wire Line
	8400 4400 8250 4400
Wire Wire Line
	8400 4650 8250 4650
Text Label 8250 4650 2    50   ~ 0
GND
$Comp
L Device:R R?
U 1 1 60B61A00
P 9750 4400
AR Path="/60B61A00" Ref="R?"  Part="1" 
AR Path="/608585CB/60B61A00" Ref="R8"  Part="1" 
F 0 "R8" H 9820 4446 50  0000 L CNN
F 1 "68k" H 9820 4355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9680 4400 50  0001 C CNN
F 3 "~" H 9750 4400 50  0001 C CNN
	1    9750 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 5050 9750 5200
Wire Wire Line
	8400 4550 8250 4550
Wire Wire Line
	8250 4550 8250 4650
Wire Wire Line
	9400 4150 9750 4150
Wire Wire Line
	9750 4650 9750 4750
$Comp
L Device:R R?
U 1 1 60B61A06
P 9750 4900
AR Path="/60B61A06" Ref="R?"  Part="1" 
AR Path="/608585CB/60B61A06" Ref="R9"  Part="1" 
F 0 "R9" H 9820 4946 50  0000 L CNN
F 1 "33k" H 9820 4855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9680 4900 50  0001 C CNN
F 3 "~" H 9750 4900 50  0001 C CNN
	1    9750 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 4250 9750 4150
Connection ~ 9750 4150
Wire Wire Line
	9750 4150 10150 4150
Wire Wire Line
	9750 4550 9750 4650
Wire Wire Line
	9750 4650 9400 4650
Connection ~ 9750 4650
Text Label 9750 5200 2    50   ~ 0
GND
$Comp
L Device:C C?
U 1 1 60B9D483
P 10150 4400
AR Path="/60B9D483" Ref="C?"  Part="1" 
AR Path="/608585CB/60B9D483" Ref="C14"  Part="1" 
F 0 "C14" H 9850 4450 50  0000 L CNN
F 1 "15 pF" H 9800 4350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10188 4250 50  0001 C CNN
F 3 "~" H 10150 4400 50  0001 C CNN
	1    10150 4400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10150 4150 10150 4250
Wire Wire Line
	10150 4550 10150 4650
Wire Wire Line
	10150 4650 9750 4650
Wire Wire Line
	10150 4150 10650 4150
Connection ~ 10150 4150
$Comp
L Device:C C?
U 1 1 60BBC161
P 10650 4400
AR Path="/60BBC161" Ref="C?"  Part="1" 
AR Path="/608585CB/60BBC161" Ref="C24"  Part="1" 
F 0 "C24" H 10350 4450 50  0000 L CNN
F 1 "1 uF" H 10300 4350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10688 4250 50  0001 C CNN
F 3 "~" H 10650 4400 50  0001 C CNN
	1    10650 4400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10650 4250 10650 4150
Wire Wire Line
	10650 4550 10650 4650
Text Label 10650 4650 0    50   ~ 0
GND
Text Notes 8700 3700 0    50   ~ 0
LDO\nIN=4.5V\nOUT=3.8V
$Comp
L Device:R R?
U 1 1 60BE551F
P 6700 4850
AR Path="/60BE551F" Ref="R?"  Part="1" 
AR Path="/608585CB/60BE551F" Ref="R6"  Part="1" 
F 0 "R6" H 6770 4896 50  0000 L CNN
F 1 "1M" H 6770 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6630 4850 50  0001 C CNN
F 3 "~" H 6700 4850 50  0001 C CNN
	1    6700 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4700 6700 4150
Wire Wire Line
	6700 5000 6700 5450
Wire Wire Line
	6250 4600 6250 4750
Wire Wire Line
	6250 4300 6250 4150
$Comp
L Device:C C?
U 1 1 60BF3119
P 6250 4450
AR Path="/60BF3119" Ref="C?"  Part="1" 
AR Path="/608585CB/60BF3119" Ref="C9"  Part="1" 
F 0 "C9" H 5950 4500 50  0000 L CNN
F 1 "100 uF" H 5900 4400 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-20_Kemet-V" H 6288 4300 50  0001 C CNN
F 3 "~" H 6250 4450 50  0001 C CNN
	1    6250 4450
	-1   0    0    -1  
$EndComp
Text Label 6250 4750 0    50   ~ 0
GND
Wire Wire Line
	4700 5450 6700 5450
Text Label 5400 4150 2    50   ~ 0
OUT_DC_DC
Text Label 6000 5450 0    50   ~ 0
LBO_DC_DC
Text Label 9950 4150 2    50   ~ 0
OUT_LDO
Text Label 8250 4400 2    50   ~ 0
EN_LDO
Wire Wire Line
	5750 4150 6250 4150
Text Label 2550 2850 0    50   ~ 0
VBATT
Text Label 2550 3050 0    50   ~ 0
OUT_LDO
Text Label 2550 3350 0    50   ~ 0
EN_LDO
$Comp
L custom:ADP161 U?
U 1 1 60A6BA02
P 5900 1900
AR Path="/60A6BA02" Ref="U?"  Part="1" 
AR Path="/608585CB/60A6BA02" Ref="U7"  Part="1" 
F 0 "U7" H 5900 2337 60  0000 C CNN
F 1 "ADP161" H 5900 2231 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 5900 2231 60  0001 C CNN
F 3 "" H 5900 1900 60  0000 C CNN
	1    5900 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60A6BA03
P 4700 2100
AR Path="/60A6BA03" Ref="C?"  Part="1" 
AR Path="/608585CB/60A6BA03" Ref="C10"  Part="1" 
F 0 "C10" H 4815 2146 50  0000 L CNN
F 1 "1uF" H 4815 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4738 1950 50  0001 C CNN
F 3 "~" H 4700 2100 50  0001 C CNN
	1    4700 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60A6BA04
P 7000 2100
AR Path="/60A6BA04" Ref="C?"  Part="1" 
AR Path="/608585CB/60A6BA04" Ref="C11"  Part="1" 
F 0 "C11" H 7115 2146 50  0000 L CNN
F 1 "1uF" H 7115 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7038 1950 50  0001 C CNN
F 3 "~" H 7000 2100 50  0001 C CNN
	1    7000 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 1800 4700 1950
Wire Wire Line
	4700 1800 5000 1800
Wire Wire Line
	5400 1950 5250 1950
Wire Wire Line
	7000 1800 7000 1950
$Comp
L Device:R R?
U 1 1 608BCEFB
P 6600 1950
AR Path="/608BCEFB" Ref="R?"  Part="1" 
AR Path="/608585CB/608BCEFB" Ref="R7"  Part="1" 
F 0 "R7" H 6670 1996 50  0000 L CNN
F 1 "1M" H 6670 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6530 1950 50  0001 C CNN
F 3 "~" H 6600 1950 50  0001 C CNN
	1    6600 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 608BCF01
P 6600 2500
AR Path="/608BCF01" Ref="R?"  Part="1" 
AR Path="/608585CB/608BCF01" Ref="R12"  Part="1" 
F 0 "R12" H 6670 2546 50  0000 L CNN
F 1 "1M" H 6670 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6530 2500 50  0001 C CNN
F 3 "~" H 6600 2500 50  0001 C CNN
	1    6600 2500
	1    0    0    -1  
$EndComp
Text Label 5250 1950 2    50   ~ 0
GND
Wire Wire Line
	4700 2250 4700 2400
Wire Wire Line
	6600 2650 6600 2800
Wire Wire Line
	7000 2250 7000 2400
Wire Wire Line
	5000 2100 5000 1800
Wire Wire Line
	5000 2100 5400 2100
Connection ~ 5000 1800
Wire Wire Line
	5000 1800 5400 1800
Wire Wire Line
	6600 2250 6600 2350
Wire Wire Line
	6400 2050 6500 2050
Wire Wire Line
	6500 2250 6600 2250
Wire Wire Line
	6500 2050 6500 2250
Wire Wire Line
	6400 1800 6600 1800
Wire Wire Line
	6600 2100 6600 2250
Connection ~ 6600 2250
Wire Wire Line
	6600 1800 7000 1800
Connection ~ 6600 1800
Text Notes 6850 1750 0    59   ~ 0
2V 
Text Notes 5250 1400 0    50   ~ 0
Accelerometer power supply LDO\nIN=3V\nOUT=2V
Text Label 4700 1800 2    60   ~ 0
VBATT
$Comp
L Diode:1N4007 D1
U 1 1 61A20D65
P 7150 4150
F 0 "D1" H 7150 3934 50  0000 C CNN
F 1 "1N4007" H 7150 4025 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 7150 3975 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 7150 4150 50  0001 C CNN
F 4 "C260857" H 7150 4150 50  0001 C CNN "MPN"
F 5 "PS" H 7150 4150 50  0001 C CNN "FUNCTION"
	1    7150 4150
	-1   0    0    1   
$EndComp
Connection ~ 6250 4150
Wire Wire Line
	6250 4150 6700 4150
Connection ~ 6700 4150
Text Label 4700 2400 3    60   ~ 0
GND
Text Label 6600 2800 3    60   ~ 0
GND
Text Label 7000 2400 3    60   ~ 0
GND
Text HLabel 2400 3150 0    50   Output ~ 0
VOUT_A
Wire Wire Line
	2400 3150 2550 3150
Text Label 2550 3150 0    50   ~ 0
OUT_A
Text Label 7000 1800 0    50   ~ 0
OUT_A
Text Label 2550 3250 0    60   ~ 0
EN_DC_DC
Text Label 2950 4650 2    60   ~ 0
EN_DC_DC
Text Label 2950 5550 2    60   ~ 0
SYNC
Wire Wire Line
	6700 4150 7000 4150
Wire Wire Line
	7300 4150 7600 4150
Wire Wire Line
	7600 4150 8400 4150
Connection ~ 7600 4150
Text Label 7750 4150 0    50   ~ 0
LDO_IN
$Comp
L Device:R R?
U 1 1 60A921E8
P 2050 5350
AR Path="/60A921E8" Ref="R?"  Part="1" 
AR Path="/608585CB/60A921E8" Ref="R2"  Part="1" 
F 0 "R2" H 2120 5396 50  0000 L CNN
F 1 "330k" H 2120 5305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1980 5350 50  0001 C CNN
F 3 "~" H 2050 5350 50  0001 C CNN
	1    2050 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60A921E2
P 2050 4750
AR Path="/60A921E2" Ref="R?"  Part="1" 
AR Path="/608585CB/60A921E2" Ref="R1"  Part="1" 
F 0 "R1" H 2120 4796 50  0000 L CNN
F 1 "1M" H 2120 4705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1980 4750 50  0001 C CNN
F 3 "~" H 2050 4750 50  0001 C CNN
	1    2050 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60AB36F1
P 1600 4750
AR Path="/60AB36F1" Ref="C?"  Part="1" 
AR Path="/608585CB/60AB36F1" Ref="C6"  Part="1" 
F 0 "C6" H 1300 4800 50  0000 L CNN
F 1 "100 nF" H 1250 4700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1638 4600 50  0001 C CNN
F 3 "~" H 1600 4750 50  0001 C CNN
	1    1600 4750
	1    0    0    -1  
$EndComp
Text Notes 900  5850 0    50   ~ 0
REVER VALORES de R1 e R2 com limite em 2V
Text Notes 6400 3350 0    50   ~ 0
Mudar para schottky diode low forward voltage
NoConn ~ 2950 5550
Text Notes 5050 4000 0    157  ~ 0
5V
Text Notes 1000 4250 0    157  ~ 0
MIN 2V
Text Notes 7750 3800 0    157  ~ 0
4.3V
Text Notes 10000 3850 0    157  ~ 0
3.8V
$EndSCHEMATC
